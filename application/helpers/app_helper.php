<?php

function asset($file) {
	return base_url('assets/'.$file);
}

function css($asset,$tag='') {
	if (is_array($asset)){
		$link = '';
		foreach($asset as $file) {
			$link .= "<link href='".asset($file)."' rel='stylesheet' $tag>";
		}
		return $link;
	}
	return "<link href='".asset($asset)."' rel='stylesheet' $tag>";
}

function js($asset,$tag='') {
	if (is_array($asset)){
		$link = '';
		foreach($asset as $file) {
			$link .= '<script src="'.asset($file).'"></script>';
		}
		return $link;
	}
	return '<script src="'.asset($asset).'"></script>';
} 

function session($key,$value='')
{
	$ci =& get_instance();
	if ($value != ''){
		if (is_array($key)){
			$ci->session->set_userdata($key);
			return true;
		}
		$ci->session->set_userdata($key,$value);
		return true;
	}
	return $ci->session->userdata($key);
}

function generate_select_input($data,$default="",$attrs,$id=""){
	
	$option = "<select";
	foreach ($attrs as $key=>$val){
		$option .=" {$key}='{$val}'";
	}
	$option .= '>';
	if ($default != null){
		//return var_dump($default);
		$option .= "<option value=''>$default</option>";
	}
	
	if (count($data) > 0){
		foreach($data as $k=>$row){
			$selected = "";
			if (is_array($id)){
				if (in_array($k,$id)){
					$selected = 'selected';
				}
			} else {
				if($id == $k){$selected = 'selected';}
			}
			
			$option .= "<option value='".$k."' $selected>".$row."</option>";
		}
	}
	$option .= "</select>";
	return $option;
}

function clean_template($template, array $variables){

 return preg_replace_callback('#{(.*?)}#',
       function($match) use ($variables){
            $match[1]=trim($match[1],'$');
            return $variables[$match[1]];
       },
       ' '.$template.' ');

}

function load_datatable()
{
	$assets = base_url('assets');
	return '<link href="'.$assets.'/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
		    <link href="'.$assets.'/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="'.$assets.'/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
            <link href="'.$assets.'/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
			<script src="'.$assets.'/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
			<script src="'.$assets.'/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>';
}

function buildTree(Array $data, $parent = 0) {
    $tree = array();
    foreach ($data as $d) {
        if ($d['parent'] == $parent) {
            $children = buildTree($data, $d['id']);
            // set a trivial key
            if (!empty($children)) {
                $d['_children'] = $children;
            }
            $tree[] = $d;
        }
    }
    return $tree;
}

function printTree($tree, $r = 0, $p = null,$id=0,$field=array()) {
    foreach ($tree as $i => $t) {
		$selected = '';
		if ($id == $t['id']){
			$selected = 'selected';
		}
        $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
        printf("\t<option value='%d' %s>%s%s</option>\n", $t[$field[0]],$selected, $dash, $t[$field[1]]);
        if ($t['parent'] == $p) {
            // reset $r
            $r = 0;
        }
        if (isset($t['_children'])) {
            printTree($t['_children'], ++$r, $t['parent'],$id,$field);
        }
    }
}

function alert($msg,$type='success')
{
	$result = "new PNotify({
			title: '".ucfirst($type)."',
            text: '$msg',
            type: '$type',
            styling: 'bootstrap3',
			hide:true
        });";
    return $result;
}

function notifikasi($message,$type)
{
	return '<div class="alert '.$type.' alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				'.$message.'
			</div>';
}

function nama_master($id,$table,$kolom="nama")
{
	$ci =& get_instance();
	$row = $ci->db->where('id',$id)->get($table)->row();
	return @$row->$kolom;
}  


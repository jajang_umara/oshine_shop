<?php 

function _post($key,$filter='string',$default='') {
		$input = isset($_POST[$key]) ? $_POST[$key] : $default;
		return filterx($input,$filter);
}
	
function _get($key,$filter='string',$default='') {
	$input = isset($_GET[$key]) ? $_GET[$key] : $default;
	return filterx($input,$filter);
}
	
function post_all(){
	return isset($_POST) ? $_POST : false;
}
	
function INPUT_HAS($index){
	return isset($_REQUEST[$index]) ? true : false;
}
	
function filterx($var,$filter){
	if ($filter == 'int') {
		return filter_var($var,FILTER_SANITIZE_NUMBER_INT);
	} else if ($filter == 'float') {
		return filter_var($var,FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
	} else if ($filter == 'string') {
		return filter_var($var,FILTER_SANITIZE_STRING);
	} else if ($filter == 'email') {
		return filter_var($var,FILTER_SANITIZE_EMAIL);
	} else if ($filter == 'url') {
		return filter_var($var,FILTER_SANITIZE_URL);
	} else if ($filter == 'upper') {
		return strtoupper(filterx($var,'string'));
	} else if ($filter == 'lower') {
		return strtolower(filterx($var,'string'));
	} else if ($filter == 'ucfirst') {
		return ucfirst(filterx($var,'lower'));
	} else if ($filter == null) {
		return $var;
	}
}
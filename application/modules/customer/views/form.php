<form method="post" class="form-horizontal" id="fm">
	<input type="hidden" name="id" value="<?= @$id; ?>">
	
	<div class="form-group">
		<label class="col-md-3">Nama Customer</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="nama" value="<?= @$row->nama; ?>" required="true"> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Jenis</label>
		<div class="col-md-9">
			<?= generate_select_input(['Agen'=>'Agen','Reseller'=>'Reseller','Ecer'=>'Ecer'],null,array('class'=>'form-control','name'=>'jenis'),@$row->jenis); ?> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Alamat</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="alamat" value="<?= @$row->alamat; ?>" required="true"> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">No Telp</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="no_telp" value="<?= @$row->no_telp; ?>" required="true"> 
		</div>
	</div>
	
</form>


<script>
var form = $("#fm");
	form.validate();

</script>
<?php

class Customer extends USER_Controller
{
	function index()
	{
		$this->load_admin('index');
	}
	
	function data()
	{
		echo json_encode($this->customer_model->generate_datatable());
	}
	
	function form()
	{
		$id = _post('id');
		$data['id'] = $id;
		if($row = $this->customer_model->get_by_id($id)){
			$data['row'] = $row;
		}
		
		$this->load->view('form',$data);
	}
	
	function save()
	{
		$id = _post('id');
		
		$data = ['nama'=>_post('nama'),
				'jenis'=>_post('jenis'),
				'alamat'=>_post('alamat'),
				'no_telp'=>_post('no_telp')];
				
		if ($this->customer_model->upsert($data,$id)){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan";
		}
	}
	
	function delete()
	{
		if ($this->customer_model->delete(_post('id'))){
			echo 'success';
		} else {
			echo "Data Gagal dihapus";
		}
	}
}
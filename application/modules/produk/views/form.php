<link href="<?= base_url('assets/vendors/bootstrap-fileinput/css/fileinput.min.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/vendors/bootstrap-fileinput/themes/explorer-fa/theme.min.css'); ?>" rel="stylesheet">
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/js/plugins/piexif.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/themes/explorer-fa/theme.min.js'); ?>"></script>

<div class="page-title">
    <div class="title_left">
        <h3>Produk Form</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Produk</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
				<?= $this->session->flashdata('notifikasi'); ?>
                <form method="post" class="form-horizontal" id="fm" action="<?= site_url('produk/save/'.$id); ?>" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?= @$id; ?>">
					<div class="form-group">
						<label class="col-md-3">Kode Barang</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="kode_barang" value="<?= @$row->kode_barang; ?>" required="true"> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Barang</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="nama_barang" value="<?= @$row->nama_barang; ?>" required="true"> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Keterangan</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="keterangan" value="<?= @$row->keterangan; ?>" required="true"> 
						</div>
					</div>
					
					<?php if(!$row) : ?>
					<hr>
					<div class="form-group">
						<label class="col-md-6"><h4>Produk Varian</h4></label>
						<button class="btn btn-primary pull-right" onclick="addVarian()" type="button">Tambah</button>
					</div>
					<hr>
					<div class="row">
					<div class="col-md-12">
					<i class="help-block">* Untuk varian/ukuran lebih dari 1 pakai koma (,) untuk pemisah</i>
					<table class="table table-bordered" id="table-varian">
						<tr>
							<th>Nama varian</th>
							<th>Ukuran</th>
							<th>Qty</th>
							<th></th>
						</tr>
					</table>
					</div>
					</div>
					<?php endif; ?>
					<hr>
					<div class="form-group">
						<label class="col-md-6"><h4>Produk Harga</h4></label>
						<button class="btn btn-primary pull-right" onclick="addHarga()" type="button">Tambah</button>
					</div>
					<hr>
					<div class="row">
					<div class="col-md-12">
				
					<table class="table table-bordered" id="table-harga">
						<tr>
							<th>Type Harga</th>
							<th>Harga</th>
							<th></th>
						</tr>
						<?php if($prices) : foreach($prices as $price) : ?>
							<tr>
								<td><?= generate_select_input($type_harga,null,['class'=>'form-control','name'=>'id_type[]'],$price->id_type); ?></td>
								<td><input type="number" name="harga_custom[]" value="<?= $price->harga; ?>" class="form-control"></td>
								<td><button type="button" class="btn btn-danger" onclick="removeHarga(this,<?=$price->id?>)"><i class="fa fa-trash"></i></button><input type="hidden" name="id_harga[]" value="<?= $price->id; ?>"></td>
						</tr>
						<?php endforeach; endif; ?>
					</table>
					</div>
					</div>
					<hr>
					<div class="form-group">
						<label class="col-md-6"><h4>Produk Image</h4></label>
						
					</div>
					<hr>
					<div class="form-group">
						<label class="btn btn-primary btn-file">
							Pilih Gambar <input type="file" style="display: none;" id="gallery-photo-add">
						</label>
						<span id="inputImage"></span>
					</div>
					<hr>
					<div class="gallery row">
						<?php if ($row) : $images = json_decode($row->photo,true); if (count($images) > 0) : foreach ($images as $image) : ?>
							<div class="show-image">
								<img src="<?= base_url('uploads/'.$image); ?>" width="250">
								<button type="button" class="delete btn btn-danger" onclick="removeImage(this,'<?=$image;?>')"><i class="fa fa-trash"></i></button>
							</div>
						<?php endforeach; endif; endif; ?>
					</div>
					<div class="form-group pull-right">
						<button class="btn btn-success" type="submit"><i class="fa fa-save"></i> SIMPAN</button>
						<button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> BATAL</button>
					</div>
				</form>
                    
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		
		
		 var imagesPreview = function(input, placeToInsertImagePreview) {

			if (input.files) {
				var filesAmount = input.files.length;
				var validImageTypes = ["image/jpeg", "image/png"];
				for (i = 0; i < filesAmount; i++) {
					if ($.inArray(input.files[i]['type'],validImageTypes) < 0){
						alert('Bukan file Gambar'); delete input.files[i]; continue;
					}
					
					var reader = new FileReader();
					reader.inputType = input.files[i]['type'];
					
					
					reader.onload = function(event) {
						var img = new Image();
							img.onload = function(imageEvent) {   
								var canvas = document.createElement('canvas');
								var ctx = canvas.getContext("2d");
								var max_size = 500;
								var width = img.width;
								var height = img.height;
								
								if (width > height) {
									if (width > max_size) {
										height *= max_size / width;
										width = max_size;
									}
								} else {
									if (height > max_size) {
										width *= max_size / height;
										height = max_size;
									}
								}
						
								canvas.width = width;
								canvas.height = height;
								canvas.getContext('2d').drawImage(img, 0, 0, width, height);
								var dataUrl = canvas.toDataURL(event.inputType);
						
								$('<div/>').addClass('show-image').append($('<img/>').attr('src',dataUrl).attr('width',250))
											.append($('<button/>').attr('type','button').addClass('delete btn-danger btn').attr('title','Hapus Gambar').append($('<i/>').addClass('fa fa-trash')).on('click',function(){
												$(this).parent().find('img').remove();
												$(this).parent().find('input').remove();
											}))
											.append("<input type='hidden' name='files[]' value='"+dataUrl+"'>")
											.appendTo(placeToInsertImagePreview);
								
						
						}
						img.src = event.target.result;
					}

					reader.readAsDataURL(input.files[i]);
					
				}
			}

			};

		$('#gallery-photo-add').on('change', function() {
			imagesPreview(this, 'div.gallery');
		});
	})
	function addVarian()
	{
		var row = "<tr><td><input type='text' name='nama_varian[]' class='form-control'></td>"+
				  "<td><input type='text' name='ukuran[]' class='form-control'></td>"+
				  "<td><input type='checkbox' name='qty[]' class='flat' value='1'> Set quantity 1</td>"+
				  "<td><button class='btn btn-danger btn-sm' type='button' onclick='removeData(this)'><i class='fa fa-trash'></i></button></td></tr>";
		$('#table-varian').append(row);
		$("input.flat").iCheck({checkboxClass:"icheckbox_flat-green",radioClass:"iradio_flat-green"});
	}
	
	function addHarga()
	{
		var select = $('<select/>').addClass('form-control').attr('name','id_type[]');
		var typeHarga = <?= json_encode($type_harga); ?>;
		if (Object.keys(typeHarga).length > 0){
			$.each(typeHarga,function(k,v){
				select.append("<option value='"+k+"'>"+v+"</option>");
			})
		}
		var row = "<tr><td>"+select.get(0).outerHTML+"</td>"+
				  "<td><input type='number' name='harga_custom[]' class='form-control'></td>"+
				  "<td><button class='btn btn-danger btn-sm' type='button' onclick='removeData(this)'><i class='fa fa-trash'></i></button><input type='hidden' name='id_harga[]' value='0'></td></tr>";
		$('#table-harga').append(row);
		
	}
	
	function removeData(ini){
		$(ini).closest('tr').remove();
	}
	
	function removeHarga(that,id) {
		$_confirm(function(){
			$.post('<?= site_url('produk/delete_harga'); ?>',{id:id}).done(function(result){
				if (result == 'success'){
					$(that).closest('tr').remove();
				} else {
					swal(result);
				}
			})
		});
	}
	
	function removeImage(that,image) {
		$_confirm(function(){
			$.post('<?= site_url('produk/delete_image'); ?>',{id:<?=$id;?>,image:image}).done(function(result){
				if (result == 'success'){
					$(that).parent().find('img').remove();
				} else {
					swal(result);
				}
			})
		});
	}
	
	
</script>
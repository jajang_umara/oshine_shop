<?= load_datatable(); ?>
<div class="page-title">
    <div class="title_left">
        <h3>Produk</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Produk</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="<?= site_url('produk/form/0'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                      <thead>
                        <tr>
						 
                          <th>No</th>
						  <th>Kode Barang</th>
						  <th>Nama Barang</th>
						  <th>Keterangan</th>
						  <th>Photo</th>
						  <th>Aksi</th>
                        </tr>
                      </thead>
					  
					</table>
				  </div>
				</div>
	</div>
</div>



<script>
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "<?= site_url('produk/data'); ?>",
				"type":"POST",
				"data": function ( d ) {
					d.myKey = "myValue";
					
				}
			},
			"columns": [
				
				{ "data": "no","orderable":false},
				{ "data": "kode_barang" },
				{ "data": "nama_barang" },
				{ "data": "keterangan" },
				{ "data":"produk_photo"},
				{ "data": "action" ,"orderable":false},
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[1, 'asc']],
		});
	});
	
	
	
	function deleteData(id)
	{
		$_confirm(function(){
			$.post('<?= site_url('produk/delete'); ?>',{id:id},function(result){
				if (result == 'success'){
					$.alert('Data Berhasil Dihapus');
					table.draw();
				} else {
					$.alert(result,'ERROR');
				}
			})
		})
	}
</script>
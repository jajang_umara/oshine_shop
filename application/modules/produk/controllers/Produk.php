<?php

class Produk extends MY_Controller
{
	function index()
	{
		$this->load_admin('index');
	}
	
	function data()
	{
		echo json_encode($this->produk_model->generate_datatable());
	}
	
	function form($id)
	{
		$data['type_harga'] = $this->type_harga_model->dropdown('id','nama');
		$data['id'] = $id;
		$data['row'] = $this->produk_model->get_by_id($id);
		$data['prices'] = $this->produk_harga_model->where_id_produk($id)->result();
		$this->load_admin('form',$data);
	}
	
	function save($id)
	{
		
		
		$this->load->library('form_validation');
		if($id == 0) {
			$is_unique =  '|is_unique[produk.kode_barang]';
			$photo  = array();
		} else {
			$is_unique =  '';
			$row = $this->produk_model->get_by_id($id);
			$photo = json_decode($row->photo);
		}
		$this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required'.$is_unique);
		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
		
                
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('notifikasi',notifikasi(validation_errors(),'alert-danger'));
            redirect('produk/form/'.$id);
        } else {
			$data = [
				'kode_barang' => _post('kode_barang'),
				'nama_barang' => _post('nama_barang'),
				'keterangan' => _post('keterangan'),
			];
			
			$folder = date('Ymd');
			$path   = FCPATH.'uploads/'.$folder.'/';
			
		
			
			if (isset($_POST['files'])){
				if (count($_POST['files']) > 0){
					foreach ($_POST['files'] as $key=>$val){
						if (strpos($val,'data:image') === 0){
							$photo[] = $folder.'/'.$this->upload_base64($val,$path);
						}
						if ($key == 0){
							$data['produk_photo'] = $photo[0];
						}
					
					}
					
				}
			}
			
			$data['photo'] = json_encode($photo);
			
			
			
			$this->db->trans_start();
			
			$id = $this->produk_model->upsert($data,$id);
			
			$data_varian = [];
			
			if (isset($_POST['nama_varian'])){
				foreach ($_POST['nama_varian'] as $key=>$variant){
					$variants = explode(",",$variant);
					$ukuran   = explode(",",$_POST['ukuran'][$key]);
					$qty      = isset($_POST['qty'][$key]) ? $_POST['qty'][$key] : 0; 
					if(count($variants) > 0){
						foreach ($variants as $var){
							if(count($ukuran) > 0){
								foreach ($ukuran as $ukr){
									$data_varian[] = array('nama_varian'=>$var,'ukuran'=>$ukr,'qty'=>$qty,'id_produk'=>$id);
								}
							}
						}
					}
				}
				$this->produk_variant_model->insert_batch($data_varian);
			}
			
			
			
			if (isset($_POST['harga_custom'])){
				foreach ($_POST['harga_custom'] as $k=>$harga){
					$data_harga = ['id_type'=>$_POST['id_type'][$k],'harga'=>$harga,'id_produk'=>$id];
					$this->produk_harga_model->upsert($data_harga,$_POST['id_harga'][$k]);
				}
			}
			
			
			$this->db->trans_complete();
            $this->session->set_flashdata('notifikasi',notifikasi('Data Berhasil Disimpan','alert-success'));
            redirect('produk/form/'.$id);
        }
	}
	
	function delete()
	{
		$id = _post('id');
		if ($this->produk_model->delete($id)){
			exit('success');
		}
		
		echo 'Data Gagal Dihapus';
	}
	
	function delete_harga()
	{
		$id = _post('id');
		if ($this->produk_harga_model->delete($id)){
			exit('success');
		}
		
		echo 'Data Gagal Dihapus';
	}
	
	function delete_image()
	{
		$id = _post('id');
		if ($row = $this->produk_model->get_by_id($id)){
			$images = json_decode($row->photo,true);
			if (count($images) > 0){
				if (file_exists(FCPATH.'uploads/'._post('image'))){
					unlink(FCPATH.'uploads/'._post('image'));
					$new_image = array_diff($images,[_post('image')]);
					$this->produk_model->update(['photo'=>json_encode($new_image)],$id);
					exit('success');
				}
			}
		}
		
		echo "Gambar gagal dihapus";
	}
	
	function get_varian()
	{
		header("Content-Type: application/json");
		$rows = $this->produk_variant_model->where_id_produk(_post('id'))->where("qty > 0")->result();
		$harga = $this->produk_harga_model->full_result(_post('id'));
		echo json_encode(['varian'=>$rows,'harga'=>$harga]);
	}
	
	
}
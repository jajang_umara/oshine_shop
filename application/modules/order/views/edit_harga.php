<form class="form-horizontal" id="fm">
	
	<div class="form-group">
		<label class="col-md-3">CO Shopee</label>
		<div class="col-md-6">
			<input type="text" name="co_shopee" class="form-control" value="<?= $row->co_shopee ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Biaya Lain</label>
		<div class="col-md-6">
			<input type="text" name="biaya_lain" class="form-control" value="<?= $row->biaya_lain ?>">
		</div>
	</div>
	<hr>
	<div class="form-group">
		<button class="btn btn-primary" type="button" onclick="doSave()"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>
	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD',
		ignoreReadonly: true,
	})
	
	function doSave() {
		$('#fm').validate()
		if ($('#fm').valid()) {
			$.post("<?= site_url('order/edit/'.$row->id) ?>",$('#fm').serialize()).done(resp => {
				$.alert(resp);
				$('#myModal').modal('hide');
				table.draw()
			}).fail(err => {
				
			})
		}
	}
</script>
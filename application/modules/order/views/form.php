<link href="<?= base_url('assets/vendors/bootstrap-fileinput/css/fileinput.min.css'); ?>" rel="stylesheet">
<link href="<?= base_url('assets/vendors/bootstrap-fileinput/themes/explorer-fa/theme.min.css'); ?>" rel="stylesheet">
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/js/plugins/piexif.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendors/bootstrap-fileinput/themes/explorer-fa/theme.min.js'); ?>"></script>

<div class="page-title">
    <div class="title_left">
        <h3><?= $jenis == 'beli' ? 'Pembelian' : 'Penjualan' ?> Form</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row" x-data="form">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $jenis == 'beli' ? 'Pembelian' : 'Penjualan' ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
				<?= $this->session->flashdata('notifikasi'); ?>
                <form method="post" class="form-horizontal" id="fm">
					<input type="hidden" name="id" value="<?= @$id; ?>">
					<div class="form-group">
						<label class="col-md-2">Nomor Order</label>
						<div class="col-md-3">
							<input type="text" class="form-control" name="no_order" x-model="form.no_order" required="true" readonly> 
						</div>
						<label class="col-md-2">Tanggal</label>
						<div class="col-md-3">
							<div class="input-group">
								<input type="text" class="form-control datepicker" name="tanggal" readonly x-model="form.tanggal" x-ondatepick>
								<span class="input-group-btn">
									<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								
							</div>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2"><?= $jenis == 'beli' ? 'Supplier' : 'Customer' ?></label>
						<div class="col-md-3">
							 <?= generate_select_input($master,'-Pilih Salah satu-',['name' => 'supplier_customer_id','class'=>'form-control','x-model'=>'form.supplier_customer_id'],@$row->supplier_customer_id); ?>
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-success" type="button" @click="addItem"><i class="fa fa-plus"></i> TAMBAH ITEM</button>
					</div>
					<div class="form-group">
						<table class="table table-bordered">
							<tr>
								<th>Nama Produk</th>
								<th>Varian</th>
								<th>Jumlah</th>
								<th>Harga</th>
								<th>Diskon</th>
								<th>Subtotal</th>
							</tr>
							<template x-for="(item,index) in items" :key="item.varian_id">
								<tr>
									<td x-text="item.produk"></td>
									<td x-text="item.varian"></td>
									<td x-text="item.qty"></td>
									<td x-text="item.harga"></td>
									<td x-text="item.diskon"></td>
									<td x-text="item.subtotal" align="right" style="font-weight:bold"></td>
									<td><button class="btn btn-danger btn-sm" type="button" @click="deleteItem(index)"><i class="fa fa-times"></i></button></td>
								</tr>
							</template>
							<tr>
								<td colspan="5" align="right">Total</td>
								<td x-text="total" align="right" style="font-weight:bold"></td>
							</tr>
						</table>
					</div>
					
					<div class="form-group pull-right">
						<button class="btn btn-success" type="button" @click="doSave"><i class="fa fa-save"></i> SIMPAN</button>
						<button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> BATAL</button>
					</div>
				</form>
                    
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<a href="javascript:void(0)" class="modal-close pull-right" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times"></i>
					</a>
					<h4 class="modal-title" id="myModalLabel">DATA PRODUK</h4>
				</div>
			<div class="modal-body">
				<form class="form-horizontal" x-effect="subTotal">
					<div class="form-group">
						<label class="col-md-3">Produk</label>
						<div class="col-md-9">
							<select x-model="item.produk_id" class="form-control" @change="produkChange">
								<option value="">-Pilih Produk-</option>
								<?php foreach ($produk as $key=>$val) : ?>
								<option value="<?= $key ?>"><?= $val ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Varian</label>
						<div class="col-md-9">
							<select x-model="item.varian_id" class="form-control" @change="varianChange">
								<option value="">-Pilih Varian-</option>
								<template x-for="row in varianList" :key="row.id">
									<option :value="row.id" x-text="row.nama_varian+' ,size: '+row.ukuran+', Stok : '+row.qty" :data-qty="row.qty"></option>
								</template>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Harga</label>
						<div class="col-md-9">
							<select x-model="item.harga" class="form-control">
								<option value="">-Pilih Harga-</option>
								<template x-for="row in hargaList" :key="row.harga">
									<option :value="row.harga" x-text="row.nama+' [ '+row.harga+ ' ]'"></option>
								</template>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah</label>
						<div class="col-md-9">
							<select x-model="item.qty" class="form-control">
								<option value="0">-Pilih Jumlah-</option>
								<template x-for="num in Array.from({length:qty},(v, k) => k+1)" :key="num">
									<option x-text="num"></option>
								</template>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Diskon</label>
						<div class="col-md-9">
							<input type="number" class="form-control" x-model="item.diskon">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Subtotal</label>
						<div class="col-md-9">
							<input type="number" class="form-control" x-model="item.subtotal" readonly>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-rounded btn-primary" @click="doAdd">Tambah</button>
			</div>
			</div>
		</div>
	</div><!--.modal-->
</div>

<script>
	document.addEventListener('alpine:init',() => {
		Alpine.data('form',() => ({
			form: {
				id:"<?= @$id ?>",
				no_order:"<?= @$row->no_order ?>",
				tanggal:"<?= @$row->tanggal; ?>",
				supplier_customer_id:"<?= @$row->supplier_customer_id; ?>",
				total:0
			},
			items:[],
			item:{
				produk_id:0,
				produk:"",
				varian_id:0,
				varian:"",
				harga:0,
				qty:0,
				diskon:0,
				subtotal:0,
			},
			varianList:[],
			hargaList:[],
			qty:0,
			init() {
				this.$watch('form.tanggal',value => {
					//alert(value);
				});
				<?php if($id == 0) : ?>
				this.getNoOrder();
				<?php endif; ?>
			},
			getNoOrder() {
				$.get('<?= site_url("order/get_no_order/$jenis") ?>').done(resp => {
					this.form.no_order = resp;
				})
			},
			addItem() {
				$('#myModal').modal('show');
			},
			produkChange() {
				this.item.produk = this.$el.options[this.$el.selectedIndex].text;
				$.post("<?= site_url('produk/get_varian') ?>",{id:this.$event.target.value}).done(resp => {
					this.varianList = resp.varian;
					this.hargaList = resp.harga;
				}).fail(err => {
					console.log(err)
				})
			},
			varianChange() {
				this.item.varian = this.$el.options[this.$el.selectedIndex].text;
				this.qty = this.$selectData('qty');
			},
			subTotal() {
				this.item.subtotal = (this.item.harga*this.item.qty) - this.item.diskon;
			},
			doAdd() {
				this.items.push(this.item);
				this.form.total += this.item.subtotal;
				this.item = {
					produk_id:0,
					produk:"",
					varian_id:0,
					varian:"",
					harga:0,
					qty:0,
					diskon:0,
					subtotal:0,
				}
				$('#myModal').modal('hide');
			},
			deleteItem(index) {
				if(confirm("Yakin Akan Hapus Data ini?")) {
					this.form.total -= this.items[index].subtotal;
					this.items.splice(index,1);
				}
			},
			doSave() {
				if (this.items.length == 0) {
					$.alert("Item Belum ditambahkan");
					return;
				}
				run_waitMe();
				$.post('<?= site_url("order/save/$jenis/") ?>',{form:this.form,items:this.items}).done(resp => {
					stop_waitMe();
					if (resp == 'success') {
						$.alert("Data Berhasil disimpan");
						this.doBatal();
					} else {
						$.alert(resp);
					}
				}).fail(err => {
					stop_waitMe()
				})
			},
			doBatal() {
				this.form = {
					id:"<?= @$id ?>",
					no_order:"<?= @$row->no_order ?>",
					tanggal:"<?= @$row->tanggal; ?>",
					supplier_customer_id:"<?= @$row->supplier_customer_id; ?>",
					total:0
				},
				<?php if($id == 0) : ?>
				this.getNoOrder()
				<?php endif; ?>
			}
		}));
	});	
</script>
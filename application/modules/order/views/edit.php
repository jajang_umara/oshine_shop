<table class="table">
	<tr>
		<td>No Order</td>
		<td><b><?= $row->no_order ?></b></td>
		<td>Tanggal</td>
		<td><b><?= $row->tanggal ?></b></td>
	</tr>
	<tr>
		<td>Jenis</td>
		<td><b><?= $row->jenis ?></b></td>
		<td>Supplier/Customer</td>
		<td><b><?= $row->jenis == 'jual' ? nama_master($row->supplier_customer_id,'master_customer') : nama_master($row->supplier_customer_id,'master_supplier') ?></b></td>
	</tr>
	
	<tr>
		<td colspan="4">
			<table class="table table-bordered table-striped">
				<tr>
					<th>No</th>
					<th>Nama Produk</th>
					<th>Varian</th>
					<th>Ukuran</th>
					<th>Qty</th>
					<th>Harga</th>
					<th>Aksi</th>
				</tr>
				<?php $no=1; if (count($rows) > 0) : foreach ($rows as $r) : ?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $r->nama_barang ?></td>
					<td><?= $r->nama_varian ?></td>
					<td><?= $r->ukuran ?></td>
					<td><select name="qty" class="form-control" onchange="updateQty(this.value,<?= $r->id ?>)">
							<option value="0">-Pilih Qty-</option>
							<?php for($i=1; $i <= ($r->stok+$r->qty);$i++) : ?>
							<option <?php if($r->qty == $i) echo "selected"; ?>><?= $i ?></option>
							<?php endfor; ?>
						</select>
					</td>
					<td align="right"><?= number_format($r->harga) ?></td>
					<td><button class="btn btn-danger btn-sm" type="button" onclick="deleteItem(<?= $r->id ?>)"><i class="fa fa-times"></i></button></td>
				</tr>
				<?php $no++; endforeach; endif; ?>
				<tr>
					<td colspan="5" align="right">Subtotal</td>
					<td align="right"><?= number_format($row->total) ?></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<hr>
<div class="form-group">
	<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
</div>

<script>
	function deleteItem(id) {
		$_confirm(function() {
			$.post("<?= site_url('order/delete_item') ?>",{id:id,order_id:<?= $row->id ?>}).done(resp => {
				$('#myModal').modal('hide');
				$.alert(resp);
				table.draw();
			})	
		})
	}
	
	function updateQty(value,id) {
		$.post("<?= site_url('order/edit_item_save') ?>",{id:id,order_id:<?= $row->id ?>,qty:value}).done(resp => {
			$('#myModal').modal('hide');
			$.alert(resp)
			table.draw();
		})	
	}
</script>
<table class="table">
	<tr>
		<td>No Order</td>
		<td><b><?= $row->no_order ?></b></td>
		<td>Tanggal</td>
		<td><b><?= $row->tanggal ?></b></td>
	</tr>
	<tr>
		<td>Jenis</td>
		<td><b><?= $row->jenis ?></b></td>
		<td>Supplier/Customer</td>
		<td><b><?= $row->jenis == 'jual' ? nama_master($row->supplier_customer_id,'master_customer') : nama_master($row->supplier_customer_id,'master_supplier') ?></b></td>
	</tr>
	<tr>
		<td>Status Bayar</td>
		<td><b><?= $row->status_bayar == 1 ? 'Sudah Bayar' : 'Belum Bayar' ?></b></td>
		<td>Tanggal Bayar</td>
		<td><b><?= $row->tanggal_bayar ?></b></td>
	</tr>
	<tr>
		<td>Cara Bayar</td>
		<td><b><?= $row->jenis_bayar ?></b></td>
		<td>Nama Bank</td>
		<td><b><?= $row->nama_bank ?></b></td>
	</tr>
	<tr>
		<td>Status Kirim</td>
		<td><b><?= $row->status_kirim == 1 ? 'Sudah Kirim' : 'Belum Kirim' ?></b></td>
		<td>Tanggal Kirim</td>
		<td><b><?= $row->tanggal_kirim ?></b></td>
	</tr>
	<tr>
		<td>Nama Expedisi</td>
		<td colspan="3"><b><?= $row->nama_expedisi ?></b></td>
	</tr>
	<tr>
		<td colspan="4">
			<table class="table table-bordered table-striped">
				<tr>
					<th>No</th>
					<th>Nama Produk</th>
					<th>Varian</th>
					<th>Ukuran</th>
					<th>Qty</th>
					<th>Harga</th>
				</tr>
				<?php $no=1; if (count($rows) > 0) : foreach ($rows as $r) : ?>
				<tr>
					<td><?= $no ?></td>
					<td><?= $r->nama_barang ?></td>
					<td><?= $r->nama_varian ?></td>
					<td><?= $r->ukuran ?></td>
					<td><?= $r->qty ?></td>
					<td align="right"><?= number_format($r->harga) ?></td>
				</tr>
				<?php $no++; endforeach; endif; ?>
				<tr>
					<td colspan="5" align="right">Subtotal</td>
					<td align="right"><?= number_format($row->total) ?></td>
				</tr>
				<tr>
					<td colspan="5" align="right">Biaya Ongkir (+)</td>
					<td align="right"><?= number_format($row->biaya_ongkir) ?></td>
				</tr>
				<tr>
					<td colspan="5" align="right">Biaya Lain2 (+)</td>
					<td align="right"><?= number_format($row->biaya_lain) ?></td>
				</tr>
				<tr>
					<td colspan="5" align="right">CO Shopee (-)</td>
					<td align="right"><?= number_format($row->co_shopee) ?></td>
				</tr>
				<tr>
					<td colspan="5" align="right">Total</td>
					<td align="right"><?= number_format(($row->total + $row->biaya_ongkir + $row->biaya_lain ) - $row->co_shopee) ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<hr>
<div class="form-group">
	<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
</div>
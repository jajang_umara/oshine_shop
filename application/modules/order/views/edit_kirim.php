<form class="form-horizontal" id="fm">
	<div class="form-group">
		<label class="col-md-3">Status Kirim</label>
		<div class="col-md-6">
			<select class="form-control" name="status_kirim" required>
				<option value="">-Pilih Status-</option>
				<?php foreach (['Belum Kirim','Sudah Kirim'] as $key=>$val) : ?>
				<option value="<?= $key ?>" <?php if ($row->status_kirim == $key) echo "selected"; ?> ><?= $val ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Tanggal</label>
		<div class="col-md-6">
			<div class="input-group">
				<input type="text" class="form-control datepicker" name="tanggal_kirim" readonly value="<?= $row->tanggal_kirim ?>">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
				</span>
				
			</div>
			
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Nama Expedisi</label>
		<div class="col-md-6">
			<select class="form-control" name="nama_expedisi" required>
				<option value="">-Pilih Cara-</option>
				<?php foreach (['JNE','JNT','WAHANA','SICEPAT','ANTERAJA','LION','POS','TIKI','SHOPEE EXPRESS'] as $key=>$val) : ?>
				<option <?php if ($row->nama_expedisi == $val) echo "selected"; ?> ><?= $val ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Nomor Resi</label>
		<div class="col-md-6">
			<input type="text" name="no_resi" class="form-control" value="<?= $row->no_resi ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Biaya Ongkir</label>
		<div class="col-md-6">
			<input type="text" name="biaya_ongkir" class="form-control" value="<?= $row->biaya_ongkir ?>">
		</div>
	</div>
	<hr>
	<div class="form-group">
		<button class="btn btn-primary" type="button" onclick="doSave()"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>
	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD',
		ignoreReadonly: true,
	})
	
	function doSave() {
		$('#fm').validate()
		if ($('#fm').valid()) {
			$.post("<?= site_url('order/edit/'.$row->id) ?>",$('#fm').serialize()).done(resp => {
				$.alert(resp);
				$('#myModal').modal('hide');
				table.draw()
			}).fail(err => {
				
			})
		}
	}
</script>
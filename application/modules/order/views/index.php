<?= load_datatable(); ?>
<div class="page-title">
    <div class="title_left">
        <h3>Data <?= $jenis == 'beli' ? 'Pembelian' : 'Penjualan' ?></h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
	  <div class="x_title">
		<h2>Data <?= $jenis == 'beli' ? 'Pembelian' : 'Penjualan' ?> </h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li><a href="<?= site_url("order/form/$jenis/0") ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	  </div>
	  <div class="x_content">
		<div class="table-responsive">
		<table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
		  <thead>
			<tr>
			  <th>Aksi</th>
			  <th>No</th>
			  <th>No Order</th>
			  <th>Tanggal</th>
			  <th>Supplier/Customer</th>
			  <th>Status Bayar</th>
			  <th>Status Kirim</th>
			  <th>Total</th>
			</tr>
		  </thead>
		  
		</table>
		</div>
	  </div>
	</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<a href="javascript:void(0)" class="modal-close pull-right" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times"></i>
					</a>
					<h4 class="modal-title modal-title-1" id="myModalLabel">Form Supplier</h4>
				</div>
			<div class="modal-body modal-body-1">
								
			</div>
			
			</div>
		</div>
	</div><!--.modal-->

<script>
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			responsive: true,
			"ajax": {
				"url": "<?= site_url('order/data'); ?>",
				"type":"POST",
				"data": function ( d ) {
					d.jenis = "<?= $jenis ?>";
				}
			},
			"columns": [
				{ "data": "action" ,"orderable":false},
				{ "data": "no","orderable":false},
				{ "data": "no_order" },
				{ "data": "tanggal" },
				{ "data": "supplier_customer" },
				{ "data": "status_bayar" },
				{ "data": "status_kirim" },
				{ "data": "total" },
				
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[0, 'asc']],
		});
		
		$(document).click(function (event) {
        	//hide all our dropdowns
        	$('.dropdown-menu[data-parent]').hide();

    	});
	
		$(document).on('click', '#myTable [data-toggle="dropdown"]', function () {
			// if the button is inside a modal
			if ($('body').hasClass('modal-open')) {
				throw new Error("This solution is not working inside a responsive table inside a modal, you need to find out a way to calculate the modal Z-index and add it to the element")
				return true;
			}

			$buttonGroup = $(this).parent();
			if (!$buttonGroup.attr('data-attachedUl')) {
				var ts = +new Date;
				$ul = $(this).siblings('ul');
				$ul.attr('data-parent', ts);
				$buttonGroup.attr('data-attachedUl', ts);
				$(window).resize(function () {
					$ul.css('display', 'none').data('top');
				});
			} else {
				$ul = $('[data-parent=' + $buttonGroup.attr('data-attachedUl') + ']');
			}
			if (!$buttonGroup.hasClass('open')) {
				$ul.css('display', 'none');
				return;
			}
			dropDownFixPosition($(this).parent(), $ul);
			function dropDownFixPosition(button, dropdown) {
				var dropDownTop = button.offset().top + button.outerHeight();
				dropdown.css('top', dropDownTop + "px");
				dropdown.css('left', button.offset().left + "px");
				dropdown.css('position', "absolute");
	   
				dropdown.css('width', dropdown.width());
				dropdown.css('heigt', dropdown.height());
				dropdown.css('display', 'block');
				dropdown.appendTo('body');
			}
		});
	});
	
	function deleteData(id)
	{
		$_confirm(function(){
			$.post('<?= site_url('supplier/delete'); ?>',{id:id},function(result){
				if (result == 'success'){
					$.alert('Data Berhasil Dihapus');
					table.draw();
				} else {
					$.alert(result,'ERROR');
				}
			})
		})
	}
	
	function tambahItem(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("TAMBAH ITEM")
		$.post('<?= site_url("order/add_item"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
	
	function editBayar(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("EDIT PEMBAYARAN")
		$.post('<?= site_url("order/edit_bayar"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
	
	function editKirim(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("EDIT PENGIRIMAN")
		$.post('<?= site_url("order/edit_kirim"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
	
	function editHarga(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("EDIT HARGA")
		$.post('<?= site_url("order/edit_harga"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
	
	function detail(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("DETAIL ORDER")
		$.post('<?= site_url("order/detail"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
	
	function edit(id)
	{
		$('.modal-body-1').html("");
		$('.modal-title-1').html("EDIT/DELETE ITEM")
		$.post('<?= site_url("order/edit_item"); ?>',{id:id},function(result){
			$('.modal-body-1').html(result);
			$('#myModal').modal('show');
		})
		
	}
</script>
<form class="form-horizontal" x-effect="subTotal" x-data="form" id="fm">
	<div class="form-group">
		<label class="col-md-3">Produk</label>
		<div class="col-md-6">
			<select x-model="item.produk_id" class="form-control" @change="produkChange" name="produk_id" required>
				<option value="">-Pilih Produk-</option>
				<?php foreach ($produk as $key=>$val) : ?>
				<option value="<?= $key ?>"><?= $val ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Varian</label>
		<div class="col-md-6">
			<select x-model="item.varian_id" class="form-control" @change="varianChange" name="varian_id" required>
				<option value="">-Pilih Varian-</option>
				<template x-for="row in varianList" :key="row.id">
					<option :value="row.id" x-text="row.nama_varian+' ,size: '+row.ukuran+', Stok : '+row.qty" :data-qty="row.qty"></option>
				</template>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Harga</label>
		<div class="col-md-6">
			<select x-model="item.harga" class="form-control" name="harga" required>
				<option value="">-Pilih Harga-</option>
				<template x-for="row in hargaList" :key="row.harga">
					<option :value="row.harga" x-text="row.nama+' [ '+row.harga+ ' ]'"></option>
				</template>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Jumlah</label>
		<div class="col-md-6">
			<select x-model="item.qty" class="form-control">
				<option value="0">-Pilih Jumlah-</option>
				<template x-for="num in Array.from({length:qty},(v, k) => k+1)" :key="num">
					<option x-text="num"></option>
				</template>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Diskon</label>
		<div class="col-md-6">
			<input type="number" class="form-control" x-model="item.diskon">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Subtotal</label>
		<div class="col-md-6">
			<input type="number" class="form-control" x-model="item.subtotal" readonly>
		</div>
	</div>
	<hr>
	<div class="form-group">
		<button class="btn btn-primary" type="button" @click="doSave"><i class="fa fa-save"></i> Simpan</button>
		<button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
	</div>
</form>

<script>
	Alpine.data("form",() => ({
		varianList:[],
		hargaList:[],
		qty:0,
		item:{
			produk_id:0,
			produk:"",
			varian_id:0,
			varian:"",
			harga:0,
			qty:0,
			diskon:0,
			subtotal:0,
		},
		produkChange() {
			this.item.produk = this.$el.options[this.$el.selectedIndex].text;
			$.post("<?= site_url('produk/get_varian') ?>",{id:this.$event.target.value}).done(resp => {
				this.varianList = resp.varian;
				this.hargaList = resp.harga;
			}).fail(err => {
				console.log(err)
			})
		},
		varianChange() {
			this.qty = this.$selectData('qty');
		},
		subTotal() {
			this.item.subtotal = (this.item.harga*this.item.qty) - this.item.diskon;
		},
		doSave() {
			$('#fm').validate()
			if ($('#fm').valid()) {
				$.post("<?= site_url('order/save_item/'.$id) ?>",this.item).done(resp => {
					$.alert(resp);
					$('#myModal').modal('hide');
					table.draw()
				}).fail(err => {
					
				})
			}
		}
	}))
</script>
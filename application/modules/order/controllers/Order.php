<?php

class Order extends USER_Controller
{
	function index($jenis)
	{
		$this->load_admin('index',compact('jenis'));
	}
	
	function data()
	{
		echo json_encode($this->order_model->generate_datatable());
	}
	
	function form($jenis,$id)
	{
		$data['jenis'] = $jenis;
		$data['id'] = $id;
		
		if ($jenis == 'beli') {
			$data['master'] = $this->supplier_model->dropdown('id','nama');
		} else {
			$data['master'] = $this->customer_model->dropdown('id','nama');
		}
		$data['produk'] = $this->produk_model->dropdown('id','nama_barang');
		$this->load_admin('form',$data);
	}
	
	function save($jenis)
	{
		$data_order = $_POST['form'];
		$data_order['jenis'] = $jenis;
		$data_items = $_POST['items'];
		$id = $data_order['id'];
		unset($data_order['id']);
		$this->db->trans_start();
		if ($id = $this->order_model->insert($data_order)) {
			foreach($data_items as $key=>$item) {
				$items = ['id_order'=>$id,'id_produk'=>$item['produk_id'],'id_varian'=>$item['varian_id'],'diskon'=>$item['diskon'],'harga'=>$item['harga'],'qty'=>$item['qty']];
				$this->order_detail_model->insert($items);
				$this->produk_variant_model->update_stok($item['varian_id'],$item['qty'],0,$jenis);
			}
			echo "success";
			$this->db->trans_complete();
		} else {
			echo "Data Gagal Disimpan";
		}
	}
	
	function get_no_order($jenis)
	{
		echo $this->order_model->nomor_order($jenis);
	}
	
	function add_item()
	{
		$data['produk'] = $this->produk_model->dropdown('id','nama_barang');
		$data['id'] = _post('id');
		$this->load->view('add_item',$data);
	}
	
	function edit_bayar()
	{
		$data['row'] = $this->order_model->get_by_id(_post('id'));
		$this->load->view('edit_bayar',$data);
	}
	
	function edit_kirim()
	{
		$data['row'] = $this->order_model->get_by_id(_post('id'));
		$this->load->view('edit_kirim',$data);
	}
	
	function edit_harga()
	{
		$data['row'] = $this->order_model->get_by_id(_post('id'));
		$this->load->view('edit_harga',$data);
	}
	
	function save_item($id)
	{
		$item = $_POST;
		$row = $this->order_model->get_by_id($id);
		$items = ['id_order'=>$id,'id_produk'=>$item['produk_id'],'id_varian'=>$item['varian_id'],'diskon'=>$item['diskon'],'harga'=>$item['harga'],'qty'=>$item['qty']];
		if ($detail = $this->order_detail_model->where_id_order($id)->where_id_varian($item['varian_id'])->row()) {
			$this->order_detail_model->where_id($detail->id)->update(['qty' => $detail->qty+$items['qty']]);
		} else {
			$this->order_detail_model->insert($items);
		}
		
		$this->produk_variant_model->update_stok($item['varian_id'],$item['qty'],0,$row->jenis);
		$row = $this->order_model->where_id($id)->update(['total' => $row->total + _post('subtotal')]);
		echo "Data Berhasil ditambahkan";
	}
	
	function edit($id)
	{
		$this->order_model->where_id($id)->update($_POST);
		echo "Data Berhasil Disimpan";
	}
	
	function detail()
	{
		$id = _post('id');
		$data['row'] = $this->order_model->get_by_id($id);
		$data['rows'] = $this->db->where('id_order',$id)->get('view_detail_order')->result();
		$this->load->view('detail',$data);
	}
	
	function edit_item()
	{
		$id = _post('id');
		$data['row'] = $this->order_model->get_by_id($id);
		$data['rows'] = $this->db->where('id_order',$id)->get('view_detail_order')->result();
		$this->load->view('edit',$data);
	}
	
	function delete_item()
	{
		$order = $this->order_model->get_by_id(_post('order_id'));
		$detail = $this->order_detail_model->get_by_id(_post('id'));
		
		$this->order_detail_model->where_id(_post('id'))->delete();
		$this->produk_variant_model->update_stok($detail->id_varian,0,$detail->qty,$order->jenis);
		$total = $order->total - (($detail->qty*$detail->harga) - $detail->diskon);
		$this->order_model->where_id(_post('order_id'))->update(['total' => $total]);
		echo "Data Berhasil Dihapus";
		
	}
	
	function edit_item_save()
	{
		$order = $this->order_model->get_by_id(_post('order_id'));
		$detail = $this->order_detail_model->get_by_id(_post('id'));
	
		$this->order_detail_model->where_id(_post('id'))->update(['qty'=>_post('qty')]);
		$this->produk_variant_model->update_stok($detail->id_varian,_post('qty'),$detail->qty,$order->jenis);
		$total = $this->db->select("SUM((qty*harga)-diskon) as total")->where('id_order',_post('order_id'))->get('detail_order')->row();
		$newTotal = $total ? $total->total : 0;
		$this->order_model->where_id(_post('order_id'))->update(['total' => $newTotal]);
		echo "Data Berhasil Diubah";
		
	}
	
	
}
<form method="post" class="form-horizontal" id="fm">
	<input type="hidden" name="id" value="<?= @$id; ?>">
	
	<div class="form-group">
		<label class="col-md-3">Nama</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="nama" value="<?= @$row->nama; ?>" required="true"> 
		</div>
	</div>
	
	
	<div class="form-group">
		<label class="col-md-3">Min Beli</label>
		<div class="col-md-9">
			<input type="number" class="form-control" name="min_beli" value="<?= @$row->min_beli; ?>" required="true"> 
		</div>
	</div>
	
</form>


<script>
var form = $("#fm");
	form.validate();

</script>
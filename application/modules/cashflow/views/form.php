<form method="post" class="form-horizontal" id="fm">
	<input type="hidden" name="id" value="<?= @$id; ?>">
	<div class="form-group">
		<label class="col-md-3">Tanggal</label>
		<div class="col-md-6">
		<div class="input-group">
			<input type="text" class="form-control datepicker" name="tanggal" readonly x-model="form.tanggal" x-ondatepick>
			<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
			
		</div>

		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Produk</label>
		<div class="col-md-9">
			<?= generate_select_input($produk,null,['class'=>'form-control','name'=>'id_produk'],@$row->id_produk); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Varian</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="nama_varian" value="<?= @$row->nama_varian; ?>" required="true"> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Ukuran</label>
		<div class="col-md-9">
			<input type="text" class="form-control" name="ukuran" value="<?= @$row->ukuran; ?>" required="true"> 
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3">Qty</label>
		<div class="col-md-9">
			<input type="number" class="form-control" name="qty" value="<?= @$row->qty; ?>" required="true"> 
		</div>
	</div>
	
</form>


<script>
var form = $("#fm");
	form.validate();
$('.datepicker').datetimepicker({
	format: 'YYYY-MM-DD',
	ignoreReadonly: true,
});
</script>
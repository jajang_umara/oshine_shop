<?php

class Cashflow extends USER_Controller
{
	function index()
	{
		$this->load_admin('index');
	}
	
	function data()
	{
		echo json_encode($this->cashflow_model->generate_datatable());
	}
	
	function form()
	{
		$id = _post('id');
		$data['id'] = $id;
		$data['produk'] = $this->produk_model->dropdown('id','nama_barang');
		if($row = $this->produk_variant_model->get_by_id($id)){
			$data['row'] = $row;
		}
		
		$this->load->view('form',$data);
	}
	
	function save()
	{
		$id = _post('id');
		
		$data = ['id_produk'=>_post('id_produk'),
				'nama_varian'=>_post('nama_varian'),
				'ukuran'=>_post('ukuran'),
				'qty'=>_post('qty'),];
				
		if ($this->produk_variant_model->upsert($data,$id)){
			echo 'success';
		} else {
			echo "Data Gagal Disimpan";
		}
	}
	
	function delete()
	{
		if ($this->produk_variant_model->delete(_post('id'))){
			echo 'success';
		} else {
			echo "Data Gagal dihapus";
		}
	}
}
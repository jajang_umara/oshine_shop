<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>OSHINE SHOP</title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url('assets/vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
	<!-- Switchery -->
    <link href="<?= base_url('assets/vendors/switchery/dist/switchery.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/jquery-confirm/jquery-confirm.min.css'); ?>" rel="stylesheet">
	<!-- Custom Theme Style -->
    <link href="<?= base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
	  <link href="<?= base_url('assets/css/styles.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<?= base_url('assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">
	 <link href="<?= base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
	 <link href="<?= base_url('assets/vendors/waitMe/waitMe.min.css'); ?>" rel="stylesheet">
	 <script src="<?= base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
	<style>
		
		.error {
			color: #a94442;
		}
	</style>
  </head>
  <body class="<?= $no_menu ? "" : 'nav-md'; ?>">
	<div class="container body">
		<div class="main_container">
			<?= $header; ?>
			<!-- page content -->
        <div class="right_col" role="main">
			<?= $content; ?>
		</div>
	</div>
	</div>
	
	 <!-- jQuery -->
   
    <!-- Bootstrap -->
    <script src="<?= base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
    <!-- iCheck -->
    <script src="<?= base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>
	<!-- Switchery -->
    <script src="<?= base_url('assets/vendors/switchery/dist/switchery.min.js'); ?>"></script>
	 <!-- PNotify -->
    <script src="<?= base_url('assets/vendors/pnotify/dist/pnotify.js'); ?>"></script>
    <script src="<?= base_url('assets/vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>
    <script src="<?= base_url('assets/vendors/pnotify/dist/pnotify.nonblock.js'); ?>"></script>
	<!-- Parsley -->
	<script src="<?= base_url('assets/vendors/jqueryvalidate/additional-methods.min.js'); ?>"></script>
    <script src="<?= base_url('assets/vendors/jqueryvalidate/jquery.validate.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendors/jquery-confirm/jquery-confirm.min.js'); ?>"></script>
	 
	<script src="<?= base_url('assets/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendors/moment/min/moment.min.js'); ?>"></script>
	 <script src="<?= base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="<?= base_url('assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendors/waitMe/waitMe.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendors/sweetalert/sweetalert2.all.min.js'); ?>"></script>
	<!-- Custom Theme Scripts -->
	<script src="<?= base_url('assets/build/js/custom.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/alpine-extends.js'); ?>" defer></script>
	<script src="<?= base_url('assets/js/alpine.min.js'); ?>" defer></script>
	<script>
		
		
		function $_confirm(callback)
		{
			$.confirm({
				title: 'Konfirmasi!',
				content: 'Apakah anda yakin akan menghapus data ini?',
				theme: 'supervan',
				buttons: {
					confirm: callback,
					cancel: function () {
					
					},
			
				}
			});
		}
		
		jQuery.validator.setDefaults({
			highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			}
		});
		
		$(function(){
			$('.datepicker').datetimepicker({
				format: 'YYYY-MM-DD',
				ignoreReadonly: true,
			});
			 $('.timepicker').datetimepicker({
				format: 'hh:mm:ss',
				ignoreReadonly: true,
			});
		});
		
		function run_waitMe()
		{
			$('body').waitMe({
				effect : 'win8',
				text : 'Silakan Tunggu...',
				bg : 'rgba(0, 34, 51, 0.7)',
				color : '#000',
				maxSize : '',
				waitTime : -1,
				textPos : 'vertical',
				fontSize : '24px',
				source : '',
			});
				
		}
		
		function stop_waitMe()
		{
			$('body').waitMe("hide");
		}
	</script>
  </body>
</html>

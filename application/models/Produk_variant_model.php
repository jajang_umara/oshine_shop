<?php

class Produk_variant_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'produk_varian';
		$this->column_order = ['nama_barang'];
		$this->column_search = ['nama_barang'];
		$this->order = ['nama_barang'=>'asc'];
	}
	
	function generate_datatable()
	{
		$select = "$this->table.*,produk.nama_barang";
		$join   = [["produk","$this->table.id_produk=produk.id"]];
		$where  = [];
		if (_post('produk_id',0) != 0) {
			$where["$this->table.id_produk"] = _post('produk_id');
		}
		$result = $this->_get_datatable($select,$join,$where);
		$rows = $result['data'];
		if ($rows){
			$no = $_POST['start'] + 1;
			foreach ($rows as $key=>$row){
				$rows[$key]->action = $rows[$key]->action = "<a href='javascript:void(0)' class='btn btn-info btn-sm' data-toggle='tooltip' data-placement='top' title='Ubah Data' onclick='loadForm(".$row->id.")'><i class='fa fa-pencil'></i></a>
									   <button class='btn btn-danger btn-sm' data-container='table' data-toggle='tooltip' data-placement='top' title='Hapus Data' onclick='deleteData(".$row->id.")'><i class='fa fa-trash'></i></button>";
				$rows[$key]->no     = $no;
				$no++;
			}
		}
		$result['data'] = $rows;
		return $result;
	}
	
	function update_stok($id,$qty,$qty_old,$jenis)
	{
		$row = $this->get_by_id($id);
		if ($row) {
			$qty_new = $jenis == 'jual' ? ($row->qty - $qty) + $qty_old : ($row->qty + $qty) - $qty_old ;
			$this->where_id($id)->update(['qty'=>$qty_new]);			
		}
	}
}
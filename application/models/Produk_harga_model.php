<?php

class Produk_harga_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'harga';
		$this->column_order = ['nama'];
		$this->column_search = ['nama'];
		$this->order = ['nama'=>'asc'];
	}
	
	function generate_datatable()
	{
		$result = $this->_get_datatable();
		$rows = $result['data'];
		if ($rows){
			$no = $_POST['start'] + 1;
			foreach ($rows as $key=>$row){
				$rows[$key]->action = $rows[$key]->action = "<a href='javascript:void(0)' class='btn btn-info btn-sm' data-toggle='tooltip' data-placement='top' title='Ubah Data' onclick='loadForm(".$row->id.")'><i class='fa fa-pencil'></i></a>
									   <button class='btn btn-danger btn-sm' data-container='table' data-toggle='tooltip' data-placement='top' title='Hapus Data' onclick='deleteData(".$row->id.")'><i class='fa fa-trash'></i></button>";
				$rows[$key]->no     = $no;
				$no++;
			}
		}
		$result['data'] = $rows;
		return $result;
	}
	
	function full_result($produk_id=null)
	{
		$rows = $this->db->select("A.*,B.nama")->from("$this->table A")->join("master_type_harga B","A.id_type=B.id")->where('A.id_produk',$produk_id)->get()->result();
		return $rows;
	}
}
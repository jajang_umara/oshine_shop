<?php

class Produk_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'produk';
		$this->column_order = ['kode_barang','nama_barang'];
		$this->column_search = ['kode_barang','nama_barang'];
		$this->order = ['nama'=>'asc'];
	}
	
	function generate_datatable()
	{
		$result = $this->_get_datatable();
		$rows = $result['data'];
		if ($rows){
			$no = $_POST['start'] + 1;
			foreach ($rows as $key=>$row){
				$rows[$key]->action = $rows[$key]->action = "<a href='".site_url('stok/index/'.$row->id)."' class='btn btn-success btn-sm' data-toggle='tooltip' data-placement='top' title='View Stok'>Stok</a>
										<a href='".site_url('produk/form/'.$row->id)."' class='btn btn-info btn-sm' data-toggle='tooltip' data-placement='top' title='Ubah Data'><i class='fa fa-pencil'></i></a>
									   <button class='btn btn-danger btn-sm' data-container='table' data-toggle='tooltip' data-placement='top' title='Hapus Data' onclick='deleteData(".$row->id.")'><i class='fa fa-trash'></i></button>";
				$rows[$key]->no     = $no;
				$rows[$key]->produk_photo = "<img src='".base_url('uploads/'.$row->produk_photo)."' width='80'>";
				$no++;
			}
		}
		$result['data'] = $rows;
		return $result;
	}
}
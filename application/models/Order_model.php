<?php

class Order_model extends MY_Model
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'data_order';
		$this->column_order = ['tanggal'];
		$this->column_search = ['nama_expedisi'];
		$this->order = ['tanggal'=>'asc'];
	}
	
	function generate_datatable()
	{
		
		$result = $this->_get_datatable();
		$rows = $result['data'];
		if ($rows){
			$no = $_POST['start'] + 1;
			foreach ($rows as $key=>$row){
				$rows[$key]->action = '	<div class="btn-group">
										  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-disabled="true" aria-expanded="true" data-boundary="viewport">Menu
										  <span class="caret"></span></button>
										  <ul class="dropdown-menu">
											<li><a href="#" onclick="detail('.$row->id.')">Detail</a></li>
											<li><a href="#" onclick="edit('.$row->id.')">Edir/Delete Item</a></li>
											<li><a href="#" onclick="tambahItem('.$row->id.')">Tambah Item</a></li>
											<li><a href="#" onclick="editBayar('.$row->id.')">Edit Pembayaran</a></li>
											<li><a href="#" onclick="editKirim('.$row->id.')">Edit Pengiriman</a></li>
											<li><a href="#" onclick="editHarga('.$row->id.')">Edit Harga</a></li>
										  </ul>
										</div>';
				$rows[$key]->no = $no;
				$rows[$key]->supplier_customer = $row->jenis == 'jual' ? nama_master($row->supplier_customer_id,'master_customer') : nama_master($row->supplier_customer_id,'master_supplier');
				$no++;
			}
		}
		$result['data'] = $rows;
		return $result;
	}
	
	function filter()
	{
		$this->db->where('jenis',_post('jenis'));
	}
	
	function nomor_order($jenis)
	{
		$prefix = "/$jenis/oshine-shop/".date('m/Y');
		$row = $this->db->select("MAX(SUBSTRING(no_order,1,3)) as no_order")->like('no_order',$prefix,'before')->get($this->table)->row();
		$no  = $row ? (int)$row->no_order : 0;
		$no++;
		return sprintf('%03s',$no).$prefix;
	}
}
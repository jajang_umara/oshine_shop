<?php 

class MY_Model extends CI_Model 
{
	protected $table;
	protected $primaryKey = "id";
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function __call($method,$arguments) 
	{
	
		if(substr($method,0,7) == 'get_by_'){
			$field  = substr($method,7);
			$search = $arguments[0];
			$query  = $this->db->where($field,$search)->get($this->table);
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
			return false;
		}
		
		if(substr($method,0,6) == 'where_'){
			$field  = substr($method,6);
			$search = $arguments[0];
			$this->db->where($field,$search);
			return $this;
			
		}
		
		if(substr($method,0,6) == 'order_'){
			$field  = substr($method,6);
			$order = $arguments[0];
			$this->db->order_by($field,$order);
			return $this;
		}

		if(substr($method,0,4) == 'get_'){
			$result  = substr($method,4);
			$where = isset($arguments[0]) ? $arguments[0] : false; 
			$order = isset($arguments[1]) ? $arguments[1] : false; 
			$limit = isset($arguments[2]) ? $arguments[2] : false;
			
			$this->db->select('*');
			if ($where != false) {
				$this->db->where($where);
			}
			if ($order != false) {
				$this->db->order_by(key($order),$order[key($order)]);
			}
			
			if ($limit != false) {
				$this->db->offset($limit[0])->limit($limit[1]);
			}
			
			$query = $this->db->get($this->table);
			
			if ($query->num_rows() > 0)
			{
				if ($result == 'row'){
					return $query->row();
				} else if ($result == 'result') {
					return $query->result();
				} else if ($result == 'count') {
					return $query->num_rows();
				}
				
			}
			return false;
		}
		
	}
	
	function result()
	{
		return $this->db->get($this->table)->result();
	}
	
	function count()
	{
		return $this->db->get($this->table)->num_rows();
	}
	
	function result_array()
	{
		return $this->db->get($this->table)->result_array();
	}
	
	function where($where,$value="")
	{
		if (is_array($where)){
			$this->db->where($where);
		} else if (is_string($where)){
			if ($value == ''){
				$this->db->where($where);
			} else {
				$this->db->where($where,$value);
			}
			
		}
		
		return $this;
	}
	
	function like($like,$value,$after='both')
	{
		if (is_array($like)){
			$this->db->like($like);
		} else if (is_string($like)){
			$this->db->like($like,$value,$after);
		}
		
		return $this;
	}
	
	function row()
	{
		return $this->db->get($this->table)->row();
	}
	
	function update($data,$ids=null)
	{
		if ($ids != null && !is_array($ids)){
			$this->db->where($this->primaryKey,$ids);
		}
		
		if (is_array($ids)){
			$this->db->where($ids);
			
		}
		
		return $this->db->update($this->table,$data);
		
	}
	
	function insert($data)
	{
		$this->db->insert($this->table,$data);
		return $this->db->insert_id();
	}
	
	function insert_batch($data)
	{
		return $this->db->insert_batch($this->table,$data);
	}
	
	function delete($ids=null)
	{
		if ($ids != null){
			$this->db->where($this->primaryKey,$ids);
		}
		if (is_array($ids)){
			$this->db->where($ids);
		}
		$this->db->delete($this->table);
		return $ids;
	}
	
	function dropdown($value,$text,$optional=false,$separator="-")
	{
		$result = $this->result();
		$data   = array();
		if ($result){
			foreach ($result as $row){
				$opts = "";
				if ($optional != false){
					$opts = " $separator ".$row->$optional;
				}
				$data[$row->$value] = $row->$text.$opts;
			}
		}
		return $data;
	}
	
	function upsert($data,$id)
	{
		$getById = "get_by_".$this->primaryKey;
		if ($this->$getById($id)){
			$this->update($data,$id);
			return $id;
		} else {
			return $this->insert($data);
		}
	}
	
	function orderBy($sort,$order='asc')
	{
		$this->db->order_by($sort,$order);
		return $this;
	}
	
	
	
	function _get_datatables_query($select,$join,$where)
    {
        $this->db->select($select)->from($this->table);
		
		if (count($join) > 0){
			foreach($join as $jn){
				$j_type = isset($jn[2]) ? $jn[2] : 'left';
				$this->db->join($jn[0],$jn[1],$j_type);
			}
		}
		
		 $i = 0;
		if (isset($this->column_search)){
			foreach ($this->column_search as $item) // loop column 
			{
				if($_POST['search']['value']) // if datatable send POST for search
				{
                 
					if($i===0) // first loop
					{
						$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->db->or_like($item, $_POST['search']['value']);
					}
 
					if(count($this->column_search) - 1 == $i) //last loop
						$this->db->group_end(); //close bracket
				}
				$i++;
			}
		}
		
		$this->db->where($where);
		
		$this->filter();
		$this->order_by();
        
    }
	
	function order_by()
	{
		if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
	}
	
	function limit_data()
	{
		if($_POST['length'] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
	}
	
	function _get_datatable($select="*",$join=array(),$where=array())
    {
        $this->_get_datatables_query($select,$join,$where);
        $this->limit_data();
        $query = $this->db->get();
		
		$result['draw'] = intval( $_REQUEST['draw'] );
		$result['recordsFiltered'] = $this->count_filtered($select,$join,$where);
		$result['recordsTotal'] = $result['recordsFiltered'];
		$result['data'] = $query->result();
		return $result;
    }
	
	function filter()
	{
		//$this->db->where(array());
	}
	
	public function count_filtered($select,$join,$where)
    {
        $this->_get_datatables_query($select,$join,$where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
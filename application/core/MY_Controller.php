<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
	
	protected $kp;
	
	function __construct()
	{
		parent::__construct();
		$this->kp = $this->session->userdata('kodepro');
	}
	
	function fn_upload_file($file_name, $path=null, $types = 'gif|jpg|jpeg|png|pdf|zip|rar') {
		$config = array();
		$config['upload_path'] = $path == null ? FCPATH.'uploads/' : $path;
		$config['allowed_types'] = $types;
		//$config['max_size']    = '15000000';

		$this->load->library('upload');

		$this->upload->initialize($config);

		if(!is_dir($path)){
			@mkdir($path, 0777, true);
		}

		if ( $this->upload->do_upload($file_name)) {
			$data = $this->upload->data();
			return $data['file_name'];
			
		} else {
			throw new Exception($this->upload->display_errors());
		}
	}
	
	function upload_base64($img,$path=null)
	{
		if (strpos($img, 'data:image/jpeg;base64,') === 0) {
			$img = str_replace('data:image/jpeg;base64,', '', $img);  
			$ext = '.jpg';
		}
		if (strpos($img, 'data:image/png;base64,') === 0) {
			$img = str_replace('data:image/png;base64,', '', $img); 
			$ext = '.png';
		}
		
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$filename = 'IMG-'.date("YmdHis").gettimeofday()["usec"].$ext;
		$target = $path == null ? FCPATH.'uploads/' : $path;
		
		
		if(!is_dir($target)){
			$oldUmask = umask(0);
			@mkdir($target, 0777, true);
			umask($oldUmask);
		}
		
		$target .= $filename;
		
		if (file_put_contents($target, $data)) {
			return $filename;
		} else {
			return false;
		} 
	}
	
	
	
	function load_admin($view,$data=array(),$no_menu=false)
	{
		$data['header']  = $this->load->view('header',$data,true);
		$data['no_menu'] = false;
		if ($no_menu){
			$data['header']  = '';
			$data['no_menu'] = true; 
		}
		$data['content'] = $this->load->view($view,$data,true);
		$this->load->view('template',$data);
	}
	
}

class USER_Controller extends MY_Controller{
	
	function __construct()
	{
		parent::__construct();
	}
}
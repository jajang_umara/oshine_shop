document.addEventListener('alpine:init', () => {
	Alpine.directive('ondatepick', (el,{ modifiers } ) => {
		
		$(el).on('dp.change',(e) => {
			let eventName = 'input';
			
			e.target.dispatchEvent(new CustomEvent(eventName,{ detail: e.date.format("YYYY-MM-DD"), bubbles: true }));
			if (modifiers.length > 0) {
				eventName = modifiers[0];
				e.target.children[0].dispatchEvent(new CustomEvent(eventName,{ detail: e.date.format("YYYY-MM-DD"), bubbles: true }));
			}
		})
	});
	
	Alpine.magic('selectData', (el) => {
		return attr => el.options[el.selectedIndex].getAttribute("data-"+attr)
	});
});
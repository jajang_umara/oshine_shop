-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2021 at 05:23 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oshine_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `id` int(11) NOT NULL,
  `no_order` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `debet` float NOT NULL DEFAULT '0',
  `kredit` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_order`
--

CREATE TABLE `data_order` (
  `id` int(11) NOT NULL,
  `supplier_customer_id` int(11) NOT NULL,
  `no_order` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `status_bayar` int(11) NOT NULL DEFAULT '0',
  `tanggal_bayar` date NOT NULL,
  `jenis_bayar` varchar(100) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `status_kirim` int(11) DEFAULT '0',
  `tanggal_kirim` date NOT NULL,
  `nama_expedisi` varchar(100) NOT NULL,
  `no_resi` varchar(100) NOT NULL,
  `biaya_ongkir` float NOT NULL,
  `co_shopee` float NOT NULL,
  `biaya_lain` float NOT NULL,
  `diskon` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_order`
--

INSERT INTO `data_order` (`id`, `supplier_customer_id`, `no_order`, `tanggal`, `jenis`, `status_bayar`, `tanggal_bayar`, `jenis_bayar`, `nama_bank`, `status_kirim`, `tanggal_kirim`, `nama_expedisi`, `no_resi`, `biaya_ongkir`, `co_shopee`, `biaya_lain`, `diskon`, `total`) VALUES
(3, 1, '001/jual/oshine-shop/12/2021', '2021-12-30', 'jual', 1, '2021-12-30', 'Transfer', 'BCA', 1, '2021-12-30', 'JNE', '12345678', 5000, 0, 0, 0, 70000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id` bigint(20) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_varian` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `diskon` float NOT NULL DEFAULT '0',
  `harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id`, `id_order`, `id_produk`, `id_varian`, `qty`, `diskon`, `harga`) VALUES
(3, 3, 2, 10, 2, 0, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jenis_harga` varchar(100) NOT NULL,
  `id_type` int(11) NOT NULL,
  `harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id`, `id_produk`, `jenis_harga`, `id_type`, `harga`) VALUES
(3, 2, '', 2, 25500),
(4, 2, '', 3, 30000),
(5, 2, '', 4, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `master_customer`
--

CREATE TABLE `master_customer` (
  `id` int(11) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_telp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_customer`
--

INSERT INTO `master_customer` (`id`, `jenis`, `nama`, `alamat`, `no_telp`) VALUES
(1, 'Reseller', 'customer 1', 'alamat 1', '1234567');

-- --------------------------------------------------------

--
-- Table structure for table `master_supplier`
--

CREATE TABLE `master_supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `alamat` varchar(300) NOT NULL,
  `no_telp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_supplier`
--

INSERT INTO `master_supplier` (`id`, `nama`, `alamat`, `no_telp`) VALUES
(1, 'Supplier 1', 'Alamat 12345', '1122334455');

-- --------------------------------------------------------

--
-- Table structure for table `master_type_harga`
--

CREATE TABLE `master_type_harga` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `min_beli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_type_harga`
--

INSERT INTO `master_type_harga` (`id`, `nama`, `min_beli`) VALUES
(2, 'Agen', 50),
(3, 'Reseller20', 20),
(4, 'Ecer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `produk_photo` varchar(100) NOT NULL,
  `photo` varchar(555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `kode_barang`, `nama_barang`, `keterangan`, `produk_photo`, `photo`) VALUES
(2, 'vittokid', 'vitto kidz wear', 'vitto kidz wear', '20211229/IMG-20211229031456131851.png', '[\"20211229\\/IMG-20211229031456131851.png\"]'),
(3, 'raglan', 'raglan', 'ket', '20211229/IMG-20211229031751467182.png', '[\"20211229\\/IMG-20211229031751467182.png\",\"20211229\\/IMG-20211229031832549707.png\"]');

-- --------------------------------------------------------

--
-- Table structure for table `produk_varian`
--

CREATE TABLE `produk_varian` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `nama_varian` varchar(100) NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `qty` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_varian`
--

INSERT INTO `produk_varian` (`id`, `id_produk`, `nama_varian`, `ukuran`, `qty`) VALUES
(6, 2, '1', '2', 1),
(7, 2, '1', '4', 1),
(8, 2, '1', '6', 1),
(9, 2, '1', '8', 1),
(10, 2, '1', '10', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_detail_order`
-- (See below for the actual view)
--
CREATE TABLE `view_detail_order` (
`id` bigint(20)
,`id_order` int(11)
,`id_produk` int(11)
,`id_varian` int(11)
,`qty` int(11)
,`diskon` float
,`harga` float
,`nama_barang` varchar(100)
,`nama_varian` varchar(100)
,`ukuran` varchar(20)
,`stok` float
);

-- --------------------------------------------------------

--
-- Structure for view `view_detail_order`
--
DROP TABLE IF EXISTS `view_detail_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_detail_order`  AS  select `detail_order`.`id` AS `id`,`detail_order`.`id_order` AS `id_order`,`detail_order`.`id_produk` AS `id_produk`,`detail_order`.`id_varian` AS `id_varian`,`detail_order`.`qty` AS `qty`,`detail_order`.`diskon` AS `diskon`,`detail_order`.`harga` AS `harga`,`produk`.`nama_barang` AS `nama_barang`,`produk_varian`.`nama_varian` AS `nama_varian`,`produk_varian`.`ukuran` AS `ukuran`,`produk_varian`.`qty` AS `stok` from ((`produk` join `produk_varian` on((`produk`.`id` = `produk_varian`.`id_produk`))) join `detail_order` on(((`detail_order`.`id_produk` = `produk`.`id`) and (`produk_varian`.`id` = `detail_order`.`id_varian`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_order`
--
ALTER TABLE `data_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_order` (`no_order`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`),
  ADD KEY `id_type` (`id_type`);

--
-- Indexes for table `master_customer`
--
ALTER TABLE `master_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_type_harga`
--
ALTER TABLE `master_type_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_barang` (`kode_barang`);

--
-- Indexes for table `produk_varian`
--
ALTER TABLE `produk_varian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_order`
--
ALTER TABLE `data_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_customer`
--
ALTER TABLE `master_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_supplier`
--
ALTER TABLE `master_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_type_harga`
--
ALTER TABLE `master_type_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk_varian`
--
ALTER TABLE `produk_varian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `harga_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `harga_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `master_type_harga` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `produk_varian`
--
ALTER TABLE `produk_varian`
  ADD CONSTRAINT `produk_varian_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
